Finding paths in tree: recursive -> iterative solution based on node ID ordering.

Experimenting on iris dataset and single decision tree before moving on to random forest.

Get to inner node variables: persist model to file and read into custom case classes.

When choosing impurity as node error and not including feature constraints, the original tree is found as best pruning (which is to be expected).

Pruning requires walking over the dataset again; this shouldn't be necessary ideally.

Todo:
- return w_{k,i}^{t} instead of just some features being used
--- see l.84 todo
- calculate w_{k,i} primal variable
--- 0 or 1 depending on mu_{k,i} computation, see paper page 6
- update dual variable
--- B_{k,i}^{t} <- [[ B_{k,i}^{t} + y(w_{k,i}^{t} - w_{k,i}) ]]_+ where [.]_+ = max(., 0)
- repeat until duality gap small enough
- write prediction function for PruneRF (?)
