Scala + Spark code for thesis.

Datasets are in local repository as follows:

    .
    └── data
        ├── MSLR-WEB10K
        │   ├── Fold1
        │   │   ├── test.txt
        │   │   ├── train.txt
        │   │   └── vali.txt
        │   ├── Fold2
        │   │   ├── test.txt
        │   │   ├── train.txt
        │   │   └── vali.txt
        │   ├── Fold3
        │   │   ├── test.txt
        │   │   ├── train.txt
        │   │   └── vali.txt
        │   ├── Fold4
        │   │   ├── test.txt
        │   │   ├── train.txt
        │   │   └── vali.txt
        │   └── Fold5
        │       ├── test.txt
        │       ├── train.txt
        │       └── vali.txt
        └── MSLR-WEB30K
            ├── Fold1
            │   ├── test.txt
            │   ├── train.txt
            │   └── vali.txt
            ├── Fold2
            │   ├── test.txt
            │   ├── train.txt
            │   └── vali.txt
            ├── Fold3
            │   ├── test.txt
            │   ├── train.txt
            │   └── vali.txt
            ├── Fold4
            │   ├── test.txt
            │   ├── train.txt
            │   └── vali.txt
            └── Fold5
                ├── test.txt
                ├── train.txt
                └── vali.txt
