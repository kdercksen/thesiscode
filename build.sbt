name := "RF test"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.0.2" % "provided",
  "org.apache.spark" %% "spark-mllib" % "2.0.2",
  "org.scala-lang.modules" %% "scala-pickling" % "0.10.1"
)

resolvers += "Apache HBase" at "https://repository.apache.org/content/repositories/releases"
