#!/usr/bin/env python3.4

import glob
import matplotlib.pyplot as plt
import numpy as np
import re


if __name__ == '__main__':
    names = [
        'irisrf',
        'glassrf',
        'vehiclerf',
    ]
    accs = {name: [] for name in names}
    lambdas = np.arange(0.0, 1.0, 0.05)
    for name in names:
        for filename in glob.glob('../models_prune/{}/stats*.txt'.format(name)):
            with open(filename, 'r') as f:
                line = re.search(r'^Total feature cost: (.*)', f.read(), flags=re.MULTILINE)
                accs[name].append(int(line.group(1)))

    for name in names:
        normalized = [float(i)/max(accs[name]) for i in accs[name]]
        plt.plot(lambdas, normalized)
    plt.ylabel('Total feature cost', fontsize=25)
    plt.xlabel('Tradeoff parameter', fontsize=25)
    plt.savefig('../plots/totalcost.eps', format='eps', dpi=1000)
