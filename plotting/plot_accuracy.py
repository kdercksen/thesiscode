#!/usr/bin/env python3.4

import glob
import matplotlib.pyplot as plt
import numpy as np
import re


if __name__ == '__main__':
    names = [
        'irisrf',
        'glassrf',
        'vehiclerf',
    ]
    accs = {name: [] for name in names}
    lambdas = np.arange(0.0, 1.0, 0.05)
    for name in names:
        for filename in glob.glob('../models_prune/{}/stats*.txt'.format(name)):
            with open(filename, 'r') as f:
                line = re.search(r'^Accuracy: (.*)', f.read(), flags=re.MULTILINE)
                accs[name].append(float(line.group(1)))

    for name in names:
        plt.plot(lambdas, accs[name])
    plt.xlabel('Tradeoff parameter', fontsize=25)
    plt.ylabel('Prediction accuracy', fontsize=25)
    plt.savefig('../plots/accuracy.eps', format='eps', dpi=1000)
