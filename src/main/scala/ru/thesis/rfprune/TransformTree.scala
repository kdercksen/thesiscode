package ru.thesis.rfprune

import java.io.{File, PrintWriter}
import org.apache.spark.ml.classification.DecisionTreeClassificationModel
import org.apache.spark.ml.classification.DecisionTreeClassifier
import org.apache.spark.ml.linalg.{Vector => MLVector}
import org.apache.spark.ml.tree.InternalNode
import org.apache.spark.ml.tree.LeafNode
import org.apache.spark.ml.tree.Node
import org.apache.spark.ml.tree.Split
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.{Map => MMap}
import scala.collection.mutable.Set
import scala.collection.mutable.Stack
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

case class FeatureNodePair(feature: Int, nodes: Seq[Int])
// features is a (sampleNumber, featureNumber) tuple
case class TreeSolution(treeID: Int, nodes: Seq[Int], features: Seq[(Int, Int)])

object TransformRF {

  type PrimalVariables = Array[Array[Int]]
  type DualVariables = Array[Array[Array[Double]]]

  val appName = "Transform RF into solution"
  val spark = SparkSession
    .builder()
    .appName(appName)
    .master("local")
    .getOrCreate

  spark.sparkContext.setLogLevel("ERROR")

  import spark.implicits._
  import scala.concurrent.ExecutionContext.Implicits.global
  import scala.pickling._
  import Defaults._
  import json._

  // features iris: 4
  // features glass: 10
  // features vehicle: 18

  val numFeatures = 18
  val numTrees = 20

  /*
   * Info to look at:
   * tree size, feature use (total and average/feature), prediction accuracy,
   * pruning time
   */
  def main(args: Array[String]) = {
    val irisRF = spark.read.parquet("models_noprune/vehiclerf/data")
    var irisData = spark.read.format("libsvm").load("data/vehicle/vehicle.scale")
    val numSamples = irisData.count.toInt
    val rf = TreeIO.toPruneRF(irisRF)

    val numIterations = 50
    val func = udf((v: MLVector) => v.toDense.values)

    for (lambda <- 0.0 until 1.0 by 0.05) {
      println(s"Tradeoff: ${lambda}")
      var solutions = transformRF(rf, irisData, numFeatures, Array.fill[Int](numSamples + 1, numFeatures + 1)(1), numSamples, 0)
      Util.time {
        // init code
        var duals = Array.ofDim[Double](numTrees, numSamples + 1, numFeatures + 1)
        // acquire initial w_{k,i}^t variables
        var i = 0
        while (i < numIterations) {
          println(s"Iteration:.............................. $i")
          // println("Updating primal variables...")
          val primals = updatePrimalVars(irisData, numSamples, solutions, lambda, duals)

          // println("Solving shortest path problems...")
          solutions = transformRF(rf, irisData, numFeatures, primals, numSamples, lambda)

          // println("Updating dual variables...")
          duals = updateDualVars(numSamples, primals, duals, solutions, 20)

          i += 1
        }
      }
      println("Optimization done! Running predictions...")
      val features = irisData.withColumn("featureValues", func(irisData("features"))).select("featureValues").as[Array[Double]].collect
      val labels = irisData.select("label").as[Double].collect
      val predictions = features.map { PruneRF.predict(rf, solutions, _) }

      val correct = labels.zip(predictions).filter(r => r._1 == r._2).size.toDouble / numSamples
      println(s"Correct: $correct")

      val totalFeatureCost = solutions.map { s => s.features.length }.sum
      val individualFeatureCost = Array.ofDim[Int](numFeatures + 1)
      for (fi <- 0 until numFeatures) {
        individualFeatureCost(fi) = solutions.map {_.features}.flatten.filter { case (s, f) => f == fi }.length
      }

      // tree size
      val treeSizes = for (s <- solutions) yield {
        val t = rf(s.treeID)
        s.nodes.map { n => ShortestPath.pathToNode(n, t) }.flatten.toSet.size
      }

      // Write data for this lambda to files
      // Write solutions to their own file:
      val sFile = new File(s"models_prune/vehiclerf/solutions_${lambda}.txt")
      val sFileOut = new PrintWriter(sFile)
      sFileOut.write(solutions.pickle.value)
      sFileOut.close

      // Write stats to their own file
      val statFile = new File(s"models_prune/vehiclerf/stats_${lambda}.txt")
      val statFileOut = new PrintWriter(statFile)
      statFileOut.write(s"Accuracy: ${correct}\n")
      statFileOut.write(s"Total feature cost: ${totalFeatureCost}\n")
      statFileOut.write(s"Individual feature costs: ${individualFeatureCost.pickle.value}\n")
      statFileOut.write(s"Tree sizes: ${treeSizes.pickle.value}")
      statFileOut.close
    }

    spark.stop
  }

  // True if solutions are equal, false if not
  def compareSolutions(a: Seq[TreeSolution], b: Seq[TreeSolution]): Boolean = {
    if (a.isEmpty || b.isEmpty) false
    else a.zip(b).forall {
      case (x, y) => x.nodes == y.nodes && x.features == y.features
    }
  }

  def transformRF(rf: Seq[PruneTree], data: DataFrame, numFeatures: Int, primals: PrimalVariables, numSamples: Int, lambda: Double):
    Seq[TreeSolution] = {
    val futures = for (t <- rf) yield Future { TransformTree.transform(t, data, numFeatures, primals, numSamples, lambda) }
    val transformed = Future.sequence(futures)
    Await.result(transformed, Duration.Inf)
  }

  def updateDualVars(numSamples: Int, primals: PrimalVariables, duals: DualVariables, solutions:
    Seq[TreeSolution], stepsize: Double): DualVariables = {
    def maxdot(i: Double) = i.max(0)
    val newDuals = Array.ofDim[Double](duals.length, duals(0).length, duals(0)(0).length)

    // for tree, sample, feature
    for (t <- 0 until solutions.length; s <- 0 until numSamples; f <- 0 until numFeatures) {
      val treeFUse = solutions(t).features.filter { case (x, y) => x == s && y == f }
      val tPrimal = if (treeFUse.nonEmpty) 1 else 0
      // println(s"Local: ${tPrimal}, rf: ${primals(s)(f)}")
      newDuals(t)(s)(f) = maxdot(duals(t)(s)(f) + stepsize*(tPrimal - primals(s)(f)))
    }

    newDuals
  }

  def updatePrimalVars(data: DataFrame, numSamples: Int, solutions: Seq[TreeSolution], lambda:
    Double, duals: DualVariables): PrimalVariables = {
    // init stuff
    val primals = Array.ofDim[Int](numSamples + 1, numFeatures + 1)
    val startVal = lambda / numSamples.toDouble

    // concat all solutions
    val allSolutions = solutions.flatMap (s => s.features.map {
      case (x, y) => (s.treeID, x, y)
    })

    // for each sample, for each feature
    for (s <- 0 to numSamples; f <- 1 until numFeatures) {
      // use = [(tree, sample, feature) ..]
      val use = allSolutions.filter {
        case (t, i, k) => i == s && k == f
      }
      // println(use)
      // sum all applicable values from dual variables
      val temp = use.map { case (t, i, k) => duals(t)(i)(k) }.sum
      if (startVal - temp < 0) {
        // println(s"$s;$f = 1")
        primals(s)(f) = 1
      } else {
        // println(s"$s;$f = 0")
        primals(s)(f) = 0
      }
    }

    primals
  }

}

object TransformTree {

  val spark = SparkSession.builder.getOrCreate
  import spark.implicits._
  import TransformRF._

  val edges = Array.fill[Iterable[(Int, Int, Int)]](numTrees)(null)

  def buildEdges(nMatrix: Matrix[Int]): Iterable[(Int, Int, Int)] = {
    // First build a list of edges (from, to, arcIndex).
    // arcIndex specifies the node variable bound to this edge and is also
    // used to access the nodeErrors list. from and to are the row indexes in
    // the network matrix.
    val nMatrixSparse = nMatrix.asInstanceOf[SparseMatrix[Int]]
    val numGraphNodes = nMatrix.numRows
    val numGraphEdges = nMatrix.iterator.toSeq.map(_._2.getMap.keys).flatten.max
    val edgesTree: Iterable[(Int, Int, Int)] = for ((i, row) <- SparseMatrix.transpose(nMatrixSparse)) yield {
      val from = row.getMap.find(_._2 == 1)
      // ugly check to make sure that there's values in the from/to options
      if (from.nonEmpty) {
        val to = row.getMap.find(_._2 == -1).get._1
        (from.get._1, to, i)
      } else {
        null
      }
    }
    edgesTree
  }

  // (nodes, features)
  def transform(tree: PruneTree, data: DataFrame, numFeatures: Int, primals: Array[Array[Int]], numSamples: Int, lambda: Double):
    TreeSolution = {
    val preorderIndices = nodesInPreorder(tree).map(_.id)
    val paths = findPaths(tree)
    val cMatrix = createConstraintMatrix(tree.nodes.length, paths)
    val annotatedDF = annotateFeaturesDataFrame("featuresUsed", tree, data)
    // if (tree.treeID == 0) annotatedDF.select("featuresUsed").show(100, false)
    val r = annotatedDF.select("featuresUsed").as[Seq[FeatureNodePair]]
    val fMatrix = createFeatureConstraintMatrix(r.collect, tree.nodes.length,
      numFeatures, preorderIndices)
    val completeMatrix = sortMatrix(Matrix.concat(cMatrix, fMatrix),
      tree.nodes.length, preorderIndices)
    val nMatrix = createNetworkMatrix(completeMatrix)
    if (edges(tree.treeID) == null) edges(tree.treeID) = buildEdges(nMatrix)
    val nodeErrors = tree.nodes.map(nodeError(_)).map { x => x / numSamples }
    val featErrors = primals

    val solution = ShortestPath.solve(nodeErrors, featErrors, lambda,
      edges(tree.treeID), tree.nodes.length, numFeatures, nMatrix.numRows,
      numSamples)
    val solutionNodes = solution.filter(_ < tree.nodes.length)
    // TODO: change solutionFeatures to be a list of features used for each
    // example, instead of what it is now
    val solutionFeatures = solution.filter(_ >= tree.nodes.length).map(x =>
      (((x - tree.nodes.length) / numFeatures).floor.toInt, (x - tree.nodes.length) % numFeatures)).sorted
    TreeSolution(tree.treeID, solutionNodes, solutionFeatures)
  }

  def sortMatrix(m: Matrix[Int], numTreeNodes: Int, preorderIndices:
    Seq[Int]): Matrix[Int] = {
    val orderFunc = compareSparse(_: SparseVector[Int], _: SparseVector[Int],
      numTreeNodes, preorderIndices)
    val rows = m.iterator.map(_._2).map { case x: SparseVector[Int] => x }.toSeq
    val sortedRows = rows.sortWith(orderFunc)

    var result = SparseMatrix.empty[Int]
    for ((r, i) <- sortedRows.zipWithIndex) {
      result = result.addRow(i, r)
    }
    result
  }

  def compareSparse(a: SparseVector[Int], b: SparseVector[Int], numTreeNodes:
    Int, preorderIndices: Seq[Int]): Boolean = {
    def lastNodeInSparse(v: SparseVector[Int], numTreeNodes: Int): Int = {
      v.getMap.keys.filter(_ < numTreeNodes).max
    }
    val aLeaf = lastNodeInSparse(a, numTreeNodes)
    val bLeaf = lastNodeInSparse(b, numTreeNodes)
    preorderIndices.indexOf(aLeaf) < preorderIndices.indexOf(bLeaf)
  }

  def createFeatureConstraintMatrix(samples: Seq[Seq[FeatureNodePair]],
    initialOffset: Int, numFeatures: Int,
    preorderIndices: Seq[Int]): Matrix[Int] = {
    // Initial offset is the number of nodes in the tree. This way if feature n
    // is encountered, it's placed at index n+initialOffset. The offset
    // increments with the number of features used a particular sample.
    var m = SparseMatrix.empty[Int]
    var counter = 0
    for ((ss, i) <- samples.zipWithIndex; s <- ss) {
      m = m.addRow(counter, featureNodePairToSparse(s, initialOffset + numFeatures * i))
      counter += 1
    }
    m
  }

  def featureNodePairToSparse(pair: FeatureNodePair, featureOffset: Int):
    SparseVector[Int] = {
    val ixs = pair.nodes :+ (pair.feature + featureOffset)
    SparseVector.fromTuples(
      ixs.zip(List.fill[Int](ixs.size)(1)): _*
    )
  }

  def nodeError(node: PruneNode): Double = {
    val prediction = node.prediction
    val stats = node.impurityStats
    stats.sum - stats(prediction.toInt)
  }

  def annotateFeaturesDataFrame(colName: String, tree: PruneTree, df:
    DataFrame): DataFrame = {
    val func = udf((v: MLVector) => featuresForSample(tree, v.toDense.values))
    // to combine id and array of featureUsed structs
    df.withColumn(colName, func(df("features")))
  }

  // usage: create udf((v: Vector) => featuresForSample(tree,
  // v.toDense.values)) and map it over "features" column
  // returns a [(featureIndex, [node.id, ...])] array
  // the list of node IDs indicates the path taken to the node that uses the
  // feature (used in creating the feature constraints)
  def featuresForSample(tree: PruneTree, sample: Array[Double]):
    Array[(Int, Seq[Int])] = {
    var node = tree.nodes(0)
    val features = new ArrayBuffer[(Int, Seq[Int])]()

    // binary trees have two child nodes or none, so only one check needed
    while (node.leftChild != -1) {
      val featureIndex = node.split.featureIndex
      features.append((featureIndex, ShortestPath.pathToNode(node.id, tree)))
      if (node.split.numCategories == -1) {
        // continuous split
        val threshold = node.split.leftCategoriesOrThreshold(0)
        node = sample(featureIndex) match {
          case n: Double if n <= threshold => tree.nodes(node.leftChild)
          case _ => tree.nodes(node.rightChild)
        }
      } else {
        // categorical split
        val leftCategories = node.split.leftCategoriesOrThreshold
        node = sample(featureIndex) match {
          case n: Double if leftCategories.contains(n) => tree.nodes(node.leftChild)
          case _ => tree.nodes(node.rightChild)
        }
      }
    }

    // reverse to get all elements in leaf -> root order
    // toMap to remove duplicates and only keep features+nodes where they were
    // encountered first
    // toArray speaks for itself
    // sorted to sort by node.id in ascending order
    features.reverse.toMap.toArray.sortWith(_._1 <= _._1).reverse
  }

  def createNetworkMatrix(cMatrix: Matrix[Int]): Matrix[Int] = {
    // for example with 3 rows:
    // -r1
    // r1-r2
    // r2-r3
    // r3
    var network = SparseMatrix.empty[Int]
    // inverse the first row
    network = network.addRow(0, cMatrix(0).inverted)
    network = network.addRow(cMatrix.numRows, cMatrix(cMatrix.numRows - 1))
    for (i <- 0 until cMatrix.numRows - 1) {
      network = network.addRow(i + 1, cMatrix(i) - cMatrix(i + 1))
    }
    network
  }

  def createConstraintMatrix(numCols: Int, paths: Array[Array[Int]]):
    Matrix[Int] = {
    var result = SparseMatrix.empty[Int]
    for ((p, i) <- paths.zipWithIndex) {
      val row = SparseVector.fromTuples(p.map { x => (x, 1) }: _*)
      result = result.addRow(i, row)
    }
    result
  }

  def findPaths(tree: PruneTree): Array[Array[Int]] = {
    val leafs = tree.nodes.filter(_.leftChild == -1)
    for (leaf <- leafs) yield ShortestPath.pathToNode(leaf.id, tree)
  }

  def nodesInPreorder(tree: PruneTree): Seq[PruneNode] = {
    var nodes = List[PruneNode]()

    def helper(root: PruneNode): Unit = {
      if (root.leftChild != -1) {
        helper(tree.nodes(root.leftChild))
        helper(tree.nodes(root.rightChild))
      }
      nodes = nodes :+ root
    }

    helper(tree.nodes(0))
    return nodes
  }

}

object ShortestPath {

  import TransformRF._
  // nodeErrors contains an error measure for each node.
  // nMatrix is the network matrix for the tree we're trying to solve.
  // this function returns a list of node indexes indicating which nodes
  // should be in the best tree.
  def solve(nodeErrors: Array[Double], featureErrors: Array[Array[Int]], lambda: Double,
    edges: Iterable[(Int, Int, Int)], numNodes: Int, numFeatures: Int,
    numGraphNodes: Int, numSamples: Int): Array[Int] = {
    // keep track of minimum path cost and corresponding edge and parent in this list
    val dist = Array.fill[(Double, Int, Int)](numGraphNodes)((Double.PositiveInfinity, 0, Int.MinValue))
    dist(dist.length - 1) = (0, -1, -1)
    for ((from, to, index) <- edges.toSeq.filter(_ != null).sortWith(_._1 > _._1)) {
      val error = if (index < numNodes) {
        nodeErrors(index)
      } else {
        val fT = featureErrors.transpose
        val fIndex = (index - numNodes) % numFeatures
        val e = lambda * fT(fIndex).sum / numSamples
        e
      }
      if (dist(to)._1 > dist(from)._1 + error) {
        dist(to) = (dist(from)._1 + error, index, from)
      }
    }

    val grouped = dist.groupBy(_._3).mapValues(v => v.minBy(_._1))
    //dist.map(_._3).filter(_ != Int.MinValue)
    grouped.values.toArray.map(_._2).filter(_ != -1).sorted
  }

  def pathToNode(destination: Int, tree: PruneTree): Array[Int] = {
    val path = new ArrayBuffer[Int]()
    var current = tree.nodes(0) // start at rootnode

    while (current.id != destination) {
      path.append(current.id)
      if (destination < current.rightChild) {
        current = tree.nodes(current.leftChild)
      } else {
        current = tree.nodes(current.rightChild)
      }
    }

    path.append(current.id)
    path.toArray
  }

}
