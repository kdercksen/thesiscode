package ru.thesis.rfprune

import java.io.{PrintWriter, File}
import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.tree.RandomForest
import org.apache.spark.mllib.tree.model.{DecisionTreeModel, RandomForestModel}
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Main {

  val DB = "MSLR-WEB10K"
  val numClasses = 5
  val categoricalFeaturesInfo = Map[Int, Int]()
  val numTrees = 100
  val featureSubsetStrategy = "auto"
  val impurity = "gini"
  val maxDepth = 8
  val maxBins = 64

  def main(args: Array[String]) = {
    val conf = new SparkConf().setAppName("RF Test").setMaster("local[*]")
    val sc = new SparkContext(conf)
    sc.setLogLevel("ERROR")

    // CODE HERE

    sc.stop()
  }

  private def trainStandardModels(sc: SparkContext) = {
    // Loop through folds
    println(s"Start training on $DB")

    // Acquire results for all data folds
    val results = for (i <- 1 to 5) yield {
      println(s"$i: Loading data...")
      val (trainData, valiData, testData) = loadData(sc, i)

      println(s"$i: Training random forest...")
      val model = RandomForest.trainClassifier(trainData, numClasses,
        categoricalFeaturesInfo, numTrees, featureSubsetStrategy, impurity,
        maxDepth, maxBins)

      println(s"$i: Calculating predictions for test set...")
      val predAndLabels = testData.map{point =>
          (model.predict(point.features), point.label)
      }

      println(s"$i: Creating metrics object...")
      val metrics = new MulticlassMetrics(predAndLabels)

      (i, model, metrics)
    }

    for ((i, model, metrics) <- results) {
      val accuracy = metrics.accuracy
      val cMatrix = metrics.confusionMatrix

      println(s"Fold $i:")
      println(s"Accuracy: $accuracy")

      println("Saving model to file...")
      model.save(sc, s"models/tree_fold$i")

      println("Saving metrics to file...")
      val pw = new PrintWriter(new File(s"models/tree_fold$i.metrics"))
      pw.write(s"Accuracy: $accuracy\n")
      pw.write(s"Confusion matrix:\n$cMatrix")
      pw.close()
    }
  }

  private def loadData(sc: SparkContext, fold: Int): (RDD[LabeledPoint],
    RDD[LabeledPoint], RDD[LabeledPoint]) = {
    val trainData = MLUtils.loadLibSVMFile(sc, s"data/$DB/Fold$fold/train_noqid.txt.gz")
    val valiData = MLUtils.loadLibSVMFile(sc, s"data/$DB/Fold$fold/vali_noqid.txt.gz")
    val testData = MLUtils.loadLibSVMFile(sc, s"data/$DB/Fold$fold/test_noqid.txt.gz")

    return (trainData, valiData, testData)
  }

}
