package ru.thesis.rfprune

import scala.collection.immutable.TreeMap

/**
 * A trait for immutable numeric vectors backed by
 * [[scala.collection.immutable.TreeMap]].
 */
sealed trait Vector[T] extends Iterable[T] {

  def numElements: Int

  def apply(i: Int): T

  def inverted: Vector[T]

  /**
   * Use this to iterate if you only need non-zero elements.
   */
  def getMap: TreeMap[Int, T]

  /**
   * Iterate over the vector including zero elements (slow AF due to getOrElse
   * call on every index 0 to numElements).
   */
  def iterator: Iterator[T]

  def +(that: Vector[T]): Vector[T]

  def -(that: Vector[T]): Vector[T] = this + that.inverted

}

/**
 * A trait for immutable numeric matrices, represented as rows of
 * [[ru.thesis.rfprune.Vector]].
 */
sealed trait Matrix[T] extends Iterable[(Int, Vector[T])] {

  def numRows: Int

  def apply(i: Int): Vector[T]

  def addRow(i: Int, row: Vector[T]): Matrix[T]

  def iterator: Iterator[(Int, Vector[T])]

  def isNetworkMatrix: Boolean

}

case class SparseVector[T](val vec: TreeMap[Int, T])(implicit ev: Numeric[T])
  extends Vector[T] {

  import ev._

  def numElements = vec.keys.max + 1

  def apply(i: Int) = vec.getOrElse(i, ev.zero)

  def inverted = SparseVector[T](vec.map { case (k, v) => (k, -v) })

  def getMap = vec

  def iterator: Iterator[T] = {
    for (k <- (0 until numElements).iterator) yield apply(k)
  }

  def +(that: Vector[T]) = {
    SparseVector(
      (vec ++ that.getMap.map {
        case (k, v) => (k, v + vec.getOrElse(k, ev.zero))
      }).filter(_._2 != ev.zero)
    )
  }

  override def toString = vec.mkString(", ")

}

object SparseVector {

  def fromSeq[T](a: Seq[T])(implicit ev: Numeric[T]): SparseVector[T] = {
    val tuples = a.zipWithIndex.filter(_._1 != ev.zero).map(_.swap)
    fromTuples(tuples: _*)
  }

  def fromTuples[T](a: (Int, T)*)(implicit ev: Numeric[T]): SparseVector[T] = {
    SparseVector(TreeMap(a.filter(_._2 != ev.zero): _*))
  }

  def empty[T: Numeric]: SparseVector[T] = {
    SparseVector(TreeMap.empty[Int, T])
  }

}

case class SparseMatrix[T](val rows: TreeMap[Int, Vector[T]])
  (implicit ev: Numeric[T]) extends Matrix[T] {

  def numRows = rows.keys.max + 1

  def apply(i: Int) = rows.getOrElse(i, SparseVector.empty[T])

  def addRow(i: Int, row: Vector[T]) = SparseMatrix(rows + ((i, row)))

  def iterator: Iterator[(Int, Vector[T])] = {
    for (k <- (0 until numRows).iterator) yield (k, apply(k))
  }

  def isNetworkMatrix = {
    // Take one big list with (k, v) tuples, group by k and make sure that
    // only one -1 and one 1 exist per k.
    val flattened = rows.map(_._2.getMap).flatten.groupBy(_._1)
    val counts = for ((k, v) <- flattened) yield {
      v.map(_._2).count(_ == -1) == 1 && v.map(_._2).count(_ == 1) == 1
    }
    counts.forall(_ == true)
  }

  override def toString = rows.mkString("\n")

}

object SparseMatrix {

  /**
   * Only supports immutable stuff like List, Array[Array[T]] won't work
   * because the compiler can't figure out the implicit conversion with nested
   * data structures that are mutable (I think). There is surely a way to fix
   * this with a type/view bound but I'm not sure how yet.
   */
  def fromSeq2D[T](a: Seq[Seq[T]])(implicit ev: Numeric[T]):
    SparseMatrix[T] = {
    SparseMatrix(
      TreeMap(
        a.zipWithIndex
          .map { _.swap }
          .map {
            case (i, v) => (i, SparseVector.fromSeq(v))
          }: _*
      )
    )
  }

  def empty[T: Numeric]: SparseMatrix[T] = {
    SparseMatrix(TreeMap.empty[Int, SparseVector[T]])
  }

  // TODO: this is fucking terrible.
  def transpose[T: Numeric](m: SparseMatrix[T]): SparseMatrix[T] = {
    val mRows = m.iterator.map(_._2).toSeq
    val maxIndex = mRows.map(_.getMap.keys).flatten.max
    var result = SparseMatrix.empty[T]
    for (i <- 0 to maxIndex) {
      val r = for (n <- 0 until m.numRows) yield mRows(n)(i)
      result = result.addRow(i, SparseVector.fromSeq(r))
    }
    result
  }

}

object Matrix {

  def concat[T: Numeric](a: Matrix[T], b: Matrix[T]): Matrix[T] = {
    val aSize = a.numRows
    var result = a
    for ((i, v) <- b) {
      result = result.addRow(i + aSize, v)
    }
    result
  }

}
