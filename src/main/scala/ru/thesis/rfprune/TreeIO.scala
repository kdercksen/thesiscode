package ru.thesis.rfprune

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{min, max}
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession

case class PruneSplit(
  featureIndex: Int,
  leftCategoriesOrThreshold: Array[Double],
  numCategories: Int
)

case class PruneNode(
  id: Int,
  impurity: Double,
  prediction: Double,
  impurityStats: Array[Double],
  gain: Double,
  leftChild: Int,
  rightChild: Int,
  split: PruneSplit
)

case class PruneTree(
  treeID: Int,
  nodes: Array[PruneNode]
) {

  /*
   * Given a list of leaf nodes (pruning for this tree) and a features array,
   * return the probability 
   */
  def predictWithPruning(leafsInPruning: Array[Int], features: Array[Double]): Array[Double] = {
    var currentNode = nodes(0)
    var leftSubtreePruned = false
    while (currentNode.leftChild != -1 && !leftSubtreePruned) {
      val threshold = currentNode.split.leftCategoriesOrThreshold(0)
      if (features(currentNode.split.featureIndex) <= threshold) {
        // PRUNEEED
        if (leafsInPruning.contains(currentNode.id)) {
          leftSubtreePruned = true
        } else {
          currentNode = nodes(currentNode.leftChild)
        }
      } else {
        currentNode = nodes(currentNode.rightChild)
      }
    }
    return currentNode.impurityStats
  }

}

object TreeIO {

  val spark = SparkSession.builder.getOrCreate
  import spark.implicits._

  // Takes a single node row
  def toPruneNode(r: Row): PruneNode = {
    PruneNode(
      r.getAs[Int]("id"),
      r.getAs[Double]("impurity"),
      r.getAs[Double]("prediction"),
      r.getAs[Seq[Double]]("impurityStats").toArray,
      r.getAs[Double]("gain"),
      r.getAs[Int]("leftChild"),
      r.getAs[Int]("rightChild"),
      PruneSplit(
        r.getAs[Int]("featureIndex"),
        r.getAs[Seq[Double]]("leftCategoriesOrThreshold").toArray,
        r.getAs[Int]("numCategories")
      )
    )
  }

  // Takes a dataframe representing a single tree (i.e. rows of nodes)
  def toPruneTree(id: Int, df: DataFrame): PruneTree = {
    val explodedSplitDF = df
      .withColumn("featureIndex", df("split.featureIndex"))
      .withColumn("leftCategoriesOrThreshold", df("split.leftCategoriesOrThreshold"))
      .withColumn("numCategories", df("split.numCategories"))
    PruneTree(
      id,
      explodedSplitDF.sort($"id".asc).map(toPruneNode).collect
    )
  }

  // Takes a dataframe loaded from /rfmodel/data/ directory
  def toPruneRF(df: DataFrame): IndexedSeq[PruneTree] = {
    val minID = df.select(min("treeID")).first.getAs[Int](0)
    val maxID = df.select(max("treeID")).first.getAs[Int](0)
    for (i <- minID to maxID) yield {
      toPruneTree(i, df.filter($"treeID" === i).select("nodeData.*"))
    }
  }

}

object PruneRF {

  def predict(forest: IndexedSeq[PruneTree], solutions: Seq[TreeSolution],
    features: Array[Double]): Double = {
    val predictions = for (s <- solutions) yield {
      val tree = forest(s.treeID)
      tree.predictWithPruning(s.nodes.toArray, features)
    }
    val summed = predictions.transpose.map(_.sum)
    summed.zipWithIndex.maxBy(_._1)._2
  }

}
