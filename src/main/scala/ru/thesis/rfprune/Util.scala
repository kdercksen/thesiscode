package ru.thesis.rfprune

object Util {

  def pprint[T](arg: Array[Array[T]]) = {
    for (a <- arg) {
      println(a.mkString(", "))
    }
  }

  def time[R](block: => R): R = {
    val t0 = System.nanoTime
    val result = block
    val t1 = System.nanoTime

    println(s"Elapsed time: ${(t1 - t0) / 1000000} ms")
    result
  }

}
